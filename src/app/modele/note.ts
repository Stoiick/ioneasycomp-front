export class Note {
    id: string;
    nom: string;
    type: string;
    octave: number;
    hz: number;
    hzMin: number;
    hzMax: number;
    demiTons: number; //nombre unique pour chaque note
  
}
