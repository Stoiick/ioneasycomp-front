import { Injectable, NgZone } from "@angular/core";
import { PlayButton } from '@amcharts/amcharts4/core';

@Injectable({
  providedIn: "root"
})
export class FrequenceService {
  frequency;

  constructor(private zone: NgZone) {
    navigator.getUserMedia = navigator.getUserMedia;
    const callback = stream => {
      var ctx = new AudioContext();
      var mic = ctx.createMediaStreamSource(stream);
      var analyser = ctx.createAnalyser();
      mic.connect(analyser);
      // osc.start(0);
      var data = new Uint8Array(analyser.frequencyBinCount);
      analyser.minDecibels = -45;

      const play = () => {
        analyser.getByteFrequencyData(data);
        // get fullest bin
        var idx = 0;
        for (var j = 0; j < analyser.frequencyBinCount; j++) {
          if (data[j] > data[idx]) {
            idx = j;
          }
        }

        this.frequency = (idx * ctx.sampleRate) / analyser.fftSize;
        this.zone.run(() => {
        });
        requestAnimationFrame(play);
      };
      play();
    };

    navigator.getUserMedia(
      { video: false, audio: true },
      callback,
      console.log
    )
  }

}
