import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Note } from '../modele/note';

@Injectable({
  providedIn: 'root'
})
export class ServiceNoteService {
  noteRecu: string;
  path: string;
  listeNotes: Note;
  
  constructor(private http: HttpClient, private notes: Note) { }

  
  recupererNoteService(note :string){
      //On récuperd la note provenant du front et la stock dans la variable noteRecu
      this.noteRecu = note;
  }

  construirePath(note: string, gamme: string){
    //Reset le path au début de la requête 
    this.path = "";
    this.path = 'http://localhost:8080/notes/gamme/';
    this.path = this.path + gamme + '/' + note;
    //console.log(this.path);
    return this.path;
  }

  soumettreBackService(gamme :string){
    //On récuperd la gamme provenant du front 
    //Prépare le path à envoyer 
    let pathComplete = this.construirePath(this.noteRecu, gamme);
    //On envoi la requête à notre back 
    return this.http.get(pathComplete);
    
 }

  retournerListNotes(){
    console.log("Dans retournerListNotes : " + this.listeNotes);
    return this.listeNotes;
  }

  soumettreNoteEcoute(gamme: string, note: string){
    let pathComplete = "";
    //console.log('gamme :' + gamme + 'note : ' + note)
    pathComplete = 'http://localhost:8080/notes/gamme/' + gamme + '/' + note;
    //console.log(pathComplete);
    return this.http.get(pathComplete);
  }
}
