import { TestBed } from '@angular/core/testing';

import { FrequenceService } from './frequence.service';

describe('FrequenceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FrequenceService = TestBed.get(FrequenceService);
    expect(service).toBeTruthy();
  });
});
