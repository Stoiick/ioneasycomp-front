// import { audioinput } from 'cordova-plugin-audioinput';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { FormsModule } from '@angular/forms';
import { LoginComponent } from './pages/login/login.component';
import { RegisterComponent } from './pages/register/register.component';
import { GuitarTunerComponent } from './pages/guitar-tuner/guitar-tuner.component';
import { HomeComponent } from './pages/home/home.component';
import { InformationsComponent } from './pages/informations/informations.component';
import { AccountSettingComponent } from './pages/account-setting/account-setting.component';
import { HeaderComponent } from './components/header/header.component';
import { GuitarTabComponent } from './components/guitar-tab/guitar-tab.component';
import { FormConnexionComponent } from './components/form-connexion/form-connexion.component';
import { FormRegisterComponent } from './components/form-register/form-register.component';
import { FormAccountSettingComponent } from './components/form-account-setting/form-account-setting.component';
import {MenubarModule} from 'primeng/menubar';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ButtonModule} from 'primeng/button';
import {ToastModule} from 'primeng/toast';
import { ReactiveFormsModule } from '@angular/forms';
import {MessagesModule} from 'primeng/messages';
import {MessageModule} from 'primeng/message';
import {DropdownModule} from 'primeng/dropdown';
import {PanelModule} from 'primeng/panel';
import {PasswordModule} from 'primeng/password';
import {InputTextModule} from 'primeng/inputtext';
import {MessageService} from 'primeng/api';
import { GuitarTabInformationsComponent } from './components/guitar-tab-informations/guitar-tab-informations.component';
import {CheckboxModule} from 'primeng/checkbox';
import { ServiceNoteService } from 'src/app/services/service-note.service';
import {HttpClientModule} from '@angular/common/http';
import { Note } from './modele/note';
import { EcouteTestComponent } from './components/ecoute-test/ecoute-test.component';
import {InputTextareaModule} from 'primeng/inputtextarea';
import {SplitButtonModule} from 'primeng/splitbutton';
import {CardModule} from 'primeng/card';
import {RatingModule} from 'primeng/rating';
import {MultiSelectModule} from 'primeng/multiselect';
import { FrequenceService } from './services/frequence.service';
import { TestComponent } from './components/test/test.component';
import { GaugeChartModule } from 'angular-gauge-chart';
import {MatStepperModule} from '@angular/material/stepper';
import {AccordionModule} from 'primeng/accordion';
import { IontunerComponent } from './components/iontuner/iontuner.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    GuitarTunerComponent,
    HomeComponent,
    InformationsComponent,
    AccountSettingComponent,
    HeaderComponent,
    GuitarTabComponent,
    FormConnexionComponent,
    FormRegisterComponent,
    FormAccountSettingComponent,
    GuitarTabInformationsComponent,
    EcouteTestComponent,
    TestComponent,
    IontunerComponent,
  ],
  entryComponents: [],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    MatStepperModule,
    GaugeChartModule,
    HttpClientModule,
    BrowserModule,
    AppRoutingModule,
    MenubarModule,
    BrowserModule,
    BrowserAnimationsModule,
    ButtonModule,
    ToastModule,
    ReactiveFormsModule,
    MessagesModule,
    MessageModule,
    DropdownModule,
    PanelModule,
    PasswordModule,
    InputTextModule,
    FormsModule,
    CheckboxModule,
    InputTextareaModule,
    PanelModule,
    SplitButtonModule,
    CardModule,
    RatingModule,
    MultiSelectModule,
    AccordionModule,
    AppRoutingModule,
    // audioinput,
  ],

  providers: [
    StatusBar,
    SplashScreen,
    MessageService, 
    ServiceNoteService, 
    Note, 
    FrequenceService,
    // audioinput,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
