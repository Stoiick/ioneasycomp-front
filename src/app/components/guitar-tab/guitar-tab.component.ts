import { Component, OnInit, NgZone } from '@angular/core';
import { ServiceNoteService } from 'src/app/services/service-note.service';
import { Note } from 'src/app/modele/note';
import {MessageService, Message} from 'primeng/api';
import { EcouteService } from 'src/app/services/ecoute.service';
import {SelectItem} from 'primeng/api';
import { Historique } from 'src/app/modele/historique';

@Component({
  selector: 'app-guitar-tab',
  templateUrl: './guitar-tab.component.html',
  styleUrls: ['./guitar-tab.component.scss']
})

export class GuitarTabComponent implements OnInit {
  noteSelect = "Cliquez sur une note";
  noteDetected;
  frequenceDetected = "--- Hz";
  historiques: Historique[] = [];
  historique: Historique;
  noteString: string;
  isActivated: boolean = true;
  //tableau qui contiendra les numeros de note permettant de réaliser l'affichage 
  numeroNoteProposer: string[];
  // valeur a sauvegarder 
  gammeAEnevoyer = ''; 
  noteAEnvoyer: string; 
  gammes: SelectItem[];
  selectGamme: any;
  tabNoteBoolean = new Array<boolean>(45);
  indexNote: number; 
  public listNotes : Note[];
  frequency;
  noteDetectedEcoute;
  noteRecu: Note;
  audioActivated : boolean = false;
  //argument test
  error = false;
  msgs: Message[] = [];
  ctx;
  numeroProposition = 0 ; 
  constructor(private service : ServiceNoteService, 
    private notes: Note, 
    private messageService: MessageService, 
    private zone: NgZone,
    private serviceEcoute: EcouteService) {
      
     }

  ngOnInit() {
    this.gammes = [];
    this.gammes.push({label: 'Choisissez votre gamme', value:''});
    this.gammes.push({label: 'Majeur', value:'major'});
    this.gammes.push({label: 'Mineur', value:'minor'});
    this.gammes.push({label: 'Pentatonique Mineure', value:'pentaMinor'});
    this.gammes.push({label: 'Pentatonique Majeure', value:'pentaMajor'});
  }

 

  recupererNote(note :string){
    //Mise à jour de "noteSelect"
    var reg = new RegExp('[0-9]'); 
    this.noteSelect = note.replace(reg, '');

    //Transmet la demande a recupererNoteService
    //transmet la note au service 
    this.service.recupererNoteService(note); 
    this.noteAEnvoyer = note; 
  }

  selectedGamme(event){
    //Recuperd la value du dropdown 
    this.gammeAEnevoyer = event.value;
  }

  onSoumettreBack(){
      this.resetFret(this.listNotes);
      this.initTableau();
      //console.log(this.selectedGamme);
      this.service.soumettreBackService(this.gammeAEnevoyer).subscribe(data => {
        //On récuperd les données du back sous forme de JSON 
        this.listNotes = data as Note[]; // MAj du component 
        //reset la liste de noteString 
        this.noteString = "";
        //Récuperd le nom de chaque note 
        for(let i = 0; i < this.listNotes.length ; i++){
          this.noteString += " "+ this.listNotes[i].nom; 
        }
        console.log(this.noteString); 
        this.majFret(this.listNotes);
        this.afficherHistorique(this.noteSelect, this.gammeAEnevoyer, this.noteString);
      });
    
  }

  initTableau(){
    //Initialise le tableau de notes a chaque fois qu'on soumet une note 
    for(let i=0 ; i < 46; i++){
      this.tabNoteBoolean[i] = false; 
    }
  }

  
  majFret(listNotes: Note[]){
    //BUT : On vérifie "l'id" de la note reçu, si le numero reçu = tabNoteBoolean[i] , alors on place a true
    for(let i=0; i < this.tabNoteBoolean.length ; i++){
      for(let j=0; j < listNotes.length; j++){
        if(i === listNotes[j].demiTons){
            //Si l'index i correspond au nombre listNumeroConvert[j] Alors tabNoteBoolean[i] = true + applique une class css
            this.tabNoteBoolean[i] = true; 
            document.getElementById(this.listNotes[j].nom).classList.add('notePropose');
        }
      }
    }
  }

  resetFret(listNotes: Note[]){
    if(listNotes != null){
      for(let i=0; i < listNotes.length; i++){
        document.getElementById(this.listNotes[i].nom).classList.remove('notePropose');
      }
    }
  }

  listen(){
    this.isActivated = false;
    this.audioActivated = true; 
    if(this.audioActivated == true){
      this.messageService.add({ severity:'success', summary: 'Information :', detail:'Micro Activé !'});
    navigator.getUserMedia = navigator.getUserMedia; //active micro
    const callback = (stream) => {
      this.ctx = new AudioContext();
      
      var mic = this.ctx.createMediaStreamSource(stream);
      var analyser = this.ctx.createAnalyser();
      mic.connect(analyser);
      var data = new Uint8Array(analyser.frequencyBinCount);
      analyser.minDecibels = -31;
      const play = () => {
        analyser.getByteFrequencyData(data);
        // get fullest bin
        var idx = 0;
        for (var j = 0; j < analyser.frequencyBinCount; j++) {
          if (data[j] > data[idx]) {
            idx = j;
          }
        }

        this.frequency = (idx * this.ctx.sampleRate) / analyser.fftSize; // surement ici qu'on définie la frequence
        //Ma partie : 
        if (this.frequency > 80 && this.frequency < 1150){
          this.serviceEcoute.getNoteByFrequence(this.frequency).subscribe(data =>{
            console.log(this.listNotes)
            this.noteRecu = data as Note;
            this.noteString = "";
            //console.log(this.noteRecu);
            this.noteDetectedEcoute = this.noteRecu.nom;
            //console.log(this.noteDetectedEcoute);
            this.frequenceDetected = this.frequency;
            this.resetFret(this.listNotes);
            this.initTableau();
            //console.log(this.selectedGamme);
            this.service.soumettreNoteEcoute(this.gammeAEnevoyer, this.noteDetectedEcoute).subscribe(data => {              
            //On récuperd les données du back sous forme de JSON 
            this.listNotes = data as Note[]; // MAj du component 
            //this.afficherHistorique(this.noteDetectedEcoute, this.gammeAEnevoyer, this.listNotes);
            //Récuperd le nom de chaque note 
            for(let i = 0; i < this.listNotes.length ; i++){
            this.noteString += " "+ this.listNotes[i].nom; 
            }
            this.afficherHistorique(this.noteDetectedEcoute, this.gammeAEnevoyer, this.noteString);
            this.majFret(this.listNotes);
          })
          
        
      });
        }
        // cette méthode joue un role pour afficher la variable
        this.zone.run(() => { //besoin du run mais pas du this.style,
                            //le fait d'etre dans un ngZone oblige de run() la zone
      
        });
       
        requestAnimationFrame(play);
      };
      play();
    };

    navigator.getUserMedia( // recupère video et micro si true
      { video: false, audio: true },
      callback, console.log
      );
    }
  }

  mute(){
    //Le micro n'écoute plus
    this.isActivated = true; 
    this.audioActivated = false; 
    this.messageService.add({ severity:'info', summary: 'Information :', detail:'Micro désactivé !'});
    this.ctx.close();
    //reset la fret
    //this.initTableau();
    //this.resetFret(this.listNotes);
  }
  

  onReject() {
    this.messageService.clear('c');
  }
  
  clear() {
    this.messageService.clear();
  }

  afficherHistorique(note: string, gamme: string, liste: string){
    this.numeroProposition++;
    this.historique = new Historique();
    this.historique.index = this.numeroProposition;
    this.historique.note = note;
    this.historique.gamme = gamme; 
    this.historique.listeNotes += liste; 
    console.log(this.historique);
    this.historiques.push(this.historique);
  }
}
