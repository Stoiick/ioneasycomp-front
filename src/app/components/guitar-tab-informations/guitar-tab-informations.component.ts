import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-guitar-tab-informations',
  templateUrl: './guitar-tab-informations.component.html',
  styleUrls: ['./guitar-tab-informations.component.scss']
})
export class GuitarTabInformationsComponent implements OnInit {

  aChecked: boolean = true; 
  abChecked: boolean = true; 
  bChecked: boolean = true; 
  bbChecked: boolean = true; 
  cChecked: boolean = true; 
  dChecked: boolean = true; 
  dbChecked: boolean = true; 
  eChecked: boolean = true; 
  ebChecked: boolean = true; 
  fChecked: boolean = true; 
  gChecked: boolean = true;
  gbChecked: boolean = true;
  
  constructor() { }

  ngOnInit() {
  }

  aiSChecked(){
    this.aChecked = (!this.aChecked);
  }

  abiSChecked(){
    this.abChecked = (!this.abChecked);
  }

  biSChecked(){
    this.bChecked = (!this.bChecked);
  }

  bbiSChecked(){
    this.bbChecked = (!this.bbChecked);
  }

  ciSChecked(){
    this.cChecked = (!this.cChecked);
  }

  diSChecked(){
    this.dChecked = (!this.dChecked);
  }

  dbiSChecked(){
    this.dbChecked = (!this.dbChecked);
  }


  eiSChecked(){

    this.eChecked = (!this.eChecked);
    
  }

  ebiSChecked(){
    this.ebChecked = (!this.ebChecked);
  }

  fiSChecked(){
    this.fChecked = (!this.fChecked);
  }

  giSChecked(){
    this.gChecked = (!this.gChecked);
  }

  gbiSChecked(){
    this.gbChecked = (!this.gbChecked);
  }

}
