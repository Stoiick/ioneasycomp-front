import { Component, OnInit } from '@angular/core';

import { MenuItem } from 'primeng/api/menuitem';
import {MessageService} from 'primeng/api';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor(private messageService: MessageService) { }

  items: MenuItem[];

  ngOnInit() {
      this.items = [
          {
              label: 'Mon Compte',
              icon: 'pi pi-list',
              items: [{
                      label: 'Mes informations', 
                      routerLink: ['/my-account'],
                      icon: 'pi pi-user-edit',
                      
                  },
                  {
                    label: 'Déconnexion', 
                    routerLink: ['/login'],
                    icon: 'pi pi-sign-out',
                    
                },
                    ]
          },
          {
              label: 'Guitar Tuner',
              routerLink: ['/guitar-tuner'],
              icon: 'pi pi-sliders-v',
          },
          {
              label: 'Informations',
              routerLink: ['/informations'],
              icon: 'pi pi-info-circle',
          },
          {
              label: 'Home',
              routerLink: ['/home'],
              icon: 'pi pi-home',
          },
          {separator:true},
          {
              label: 'Quit', 
              routerLink: ['/home'],
              icon: 'pi pi-fw pi-times',
          }
      ];
  }

  showConfirm(){
    this.messageService.clear();
    this.messageService.add({key: 'c', sticky: true, severity:'warn', summary:'Etes-vous sur de vouloir vous déconnecter ?', detail:'Validez pour continuer'});
  }

  onConfirm() {
    this.messageService.clear('c');
}

onReject() {
    this.messageService.clear('c');
}

}
