import { Component, OnInit } from '@angular/core';
import {MessageService} from 'primeng/api';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';


@Component({
  selector: 'app-form-connexion',
  templateUrl: './form-connexion.component.html',
  styleUrls: ['./form-connexion.component.scss'],
  providers: [MessageService]
})
export class FormConnexionComponent implements OnInit {
  
  loginForm: FormGroup;

  

  constructor(
    private formBuilder: FormBuilder,
  
  ) { }
  

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
        username: ['', Validators.required],
        password: ['', Validators.required]
    });
   

}

 

}
