import { Component, OnInit, NgZone } from '@angular/core';
import { EcouteService } from 'src/app/services/ecoute.service';
import { Note } from 'src/app/modele/note';
import { ServiceNoteService } from 'src/app/services/service-note.service';
import { ngfactoryFilePath } from '@angular/compiler/src/aot/util';

@Component({
  selector: 'app-ecoute-test',
  templateUrl: './ecoute-test.component.html',
  styleUrls: ['./ecoute-test.component.scss']
})
export class EcouteTestComponent implements OnInit {

  frequency;
  noteDetected;
  noteRecu: Note;
  constructor(private zone: NgZone, private serviceEcoute: EcouteService, private serviceNote: ServiceNoteService) {
    navigator.getUserMedia = navigator.getUserMedia; //active micro
    const callback = (stream) => {
      var ctx = new AudioContext();
      var mic = ctx.createMediaStreamSource(stream);
      var analyser = ctx.createAnalyser();
      mic.connect(analyser);
      var data = new Uint8Array(analyser.frequencyBinCount);
      analyser.minDecibels = -45;
      const play = () => {
        analyser.getByteFrequencyData(data);
        // get fullest bin
        var idx = 0;
        for (var j = 0; j < analyser.frequencyBinCount; j++) {
          if (data[j] > data[idx]) {
            idx = j;
          }
        }

        this.frequency = (idx * ctx.sampleRate) / analyser.fftSize; // surement ici qu'on définie la frequence
        //Ma partie : 
        if (this.frequency > 80 && this.frequency < 1150){
          this.serviceEcoute.getNoteByFrequence(this.frequency).subscribe(data =>{
            this.noteRecu = data as Note;
            //let gamme = this.noteRecu.octave;
            this.noteDetected += "\n\r" + this.noteRecu.nom;
          })
        }
        // cette méthode joue un role pour afficher la variable
        this.zone.run(() => { //besoin du run mais pas du this.style,
                            //le fait d'etre dans un ngZone oblige de run() la zone
      
        });
       
        requestAnimationFrame(play);
      };
      play();
    };

    navigator.getUserMedia( // recupère video et micro si true
      { video: false, audio: true },
      callback, console.log
      );
  }

  /*@HostBinding("style.background")
  get bgStyle() {
    return `hsl(${Math.floor(this.frequency)}, 60%, 30%)`;
  }*/

  ngOnInit() {
  }

}
