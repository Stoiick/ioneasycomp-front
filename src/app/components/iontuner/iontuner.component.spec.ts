import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { IontunerComponent } from './iontuner.component';

describe('IontunerComponent', () => {
  let component: IontunerComponent;
  let fixture: ComponentFixture<IontunerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IontunerComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(IontunerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
