declare let audioinput: any;

import * as am4core from '@amcharts/amcharts4/core';
import * as am4charts from '@amcharts/amcharts4/charts';
import am4themes_animated from '@amcharts/amcharts4/themes/animated';
import { Component, OnInit, NgZone } from '@angular/core';
import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
// import { File } from '@ionic-native/file/ngx';

@Component({
  selector: 'app-iontuner',
  templateUrl: './iontuner.component.html',
  styleUrls: ['./iontuner.component.scss'],
})
export class IontunerComponent implements OnInit {
    constructor(private platform: Platform, private splashScreen: SplashScreen, private zone: NgZone, private statusBar: StatusBar,
      // private file: File
      ) {}

  frequency;
  // colorSet = new am4core.ColorSet();
  colorSet = [
  am4core.color('#a4e022'),
  am4core.color('#f8faf7'),
  am4core.color('#d91a33'),
  am4core.color('#FF9671'),
  am4core.color('#1e14db'),
  am4core.color('#14d9cf')
  ];

    BUFFER_SIZE = 16384;
    ngOnInit() {
      this.platform.ready().then(() => {
        this.statusBar.styleDefault();
        this.splashScreen.hide();

        window.addEventListener('audioinput', (event: any) => {
          alert(JSON.stringify(event.data.length));
        }, false);

        audioinput.checkMicrophonePermission((hasPermission) => {
          if (hasPermission) {
            console.log('We already have permission to record.');
            this.frequency = this.startCapture();
          } else {
            // Ask the user for permission to access the microphone
            audioinput.getMicrophonePermission((hasPermission, message) => {
              if (hasPermission) {
                console.log('User granted us permission to record.');
                this.frequency = this.startCapture();
              } else {
                console.warn('User denied permission to record.');
              }
            });
          }
        });
        this.frequency = this.startCapture();
      });

      am4core.useTheme(am4themes_animated);
          // Themes end

          // create chart
      const chart = am4core.create('chartdiv', am4charts.GaugeChart);
      chart.hiddenState.properties.opacity = 0; // this makes initial fade in effect

      chart.innerRadius = -25;

          // var axis = chart.xAxes.push(new am4charts.ValueAxis());
      const axis = chart.xAxes.push(new am4charts.ValueAxis<am4charts.AxisRendererCircular>());
      axis.min = 0;
      axis.max = 335;
      axis.strictMinMax = true;
      axis.renderer.grid.template.stroke = new am4core.InterfaceColorSet().getFor(
            'background'
          );
      axis.renderer.grid.template.strokeOpacity = 0.3;

      const range0 = axis.axisRanges.create();
      range0.value = 0;
      range0.endValue = 84;
      range0.axisFill.fillOpacity = 1;
      range0.axisFill.fill = this.colorSet[1];
      range0.axisFill.zIndex = -1;

      const range1 = axis.axisRanges.create();
      range1.value = 84;
      range1.endValue = 112;
      range1.axisFill.fillOpacity = 1;
      range1.axisFill.fill = this.colorSet[2];
      range1.axisFill.zIndex = -1;

      const range2 = axis.axisRanges.create();
      range2.value = 112;
      range2.endValue = 149;
      range2.axisFill.fillOpacity = 1;
      range2.axisFill.fill = this.colorSet[3];
      range2.axisFill.zIndex = -1;

      const range3 = axis.axisRanges.create();
      range3.value = 149;
      range3.endValue = 199;
      range3.axisFill.fillOpacity = 1;
      range3.axisFill.fill = this.colorSet[4];
      range3.axisFill.zIndex = -1;

      const range4 = axis.axisRanges.create();
      range4.value = 199;
      range4.endValue = 251;
      range4.axisFill.fillOpacity = 1;
      range4.axisFill.fill = this.colorSet[5];
      range4.axisFill.zIndex = -1;

      const range5 = axis.axisRanges.create();
      range5.value = 251;
      range5.endValue = 335;
      range5.axisFill.fillOpacity = 1;
      range5.axisFill.fill = this.colorSet[0];
      range5.axisFill.zIndex = -1;

      const hand = chart.hands.push(new am4charts.ClockHand());
          // using chart.setTimeout method as the timeout will be disposed together with a chart
      chart.setTimeout(randomValue, 100);
      function randomValue() {
            hand.showValue(this.startCapture(), 200, am4core.ease.cubicOut);
            chart.setTimeout(randomValue, 100);
          }
        }
        public startCapture() {
          const son = audioinput.start({
            bufferSize: this.BUFFER_SIZE,
            streamToWebAudio: false,
            normalize: true,
            channels: audioinput.CHANNELS.MONO,
            sampleRate: audioinput.SAMPLERATE.CD_AUDIO_44100Hz,
          });
          let analyser = son.connect(son.getAudioContext());
          
          // début de méthode de recup fréquence
            // navigator.getUserMedia = navigator.getUserMedia;
          // const callback = stream => {
          //     let ctx = new AudioContext();
          //     let mic = ctx.createMediaStreamSource(stream);
          //     let analyser = ctx.createAnalyser();
          //     mic.connect(analyser);
              // osc.start(0);
          let data = new Uint8Array(analyser.frequencyBinCount);
          analyser.minDecibels = -45;

          analyser.getByteFrequencyData(data);
                // get fullest bin
          let idx = 0;
          for (let j = 0; j < analyser.frequencyBinCount; j++) {
                  if (data[j] > data[idx]) {
                    idx = j;
                  }
                }
          let a = (idx * son.sampleRate) / analyser.fftSize;
          if (a < 336) {
                  this.frequency = a;
                }
                // this.zone.run(() => {});
                // requestAnimationFrame(play);
          return this.frequency;
              }
}
        // navigator.getUserMedia(
        //   { video: false, audio: true },
        //   callback,
        //   console.log
        // );

