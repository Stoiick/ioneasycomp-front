import { Component, OnInit } from '@angular/core';
import { FrequenceService } from 'src/app/services/frequence.service';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.scss']
})
export class TestComponent implements OnInit {

  constructor(getFrequence: FrequenceService) { getFrequence}

  ngOnInit() {

  }

}
