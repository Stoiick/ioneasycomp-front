import { Component, OnInit } from '@angular/core';
import {MessageService} from 'primeng/api';
import {SelectItem} from 'primeng/api';

@Component({
  selector: 'app-form-account-setting',
  templateUrl: './form-account-setting.component.html',
  styleUrls: ['./form-account-setting.component.scss']
})
export class FormAccountSettingComponent implements OnInit {
  note: number;
  sexe: SelectItem[];
  age: SelectItem[];
  experience: SelectItem[];

  constructor(private messageService: MessageService) {}
   
  
  ngOnInit() {
    //Initialisation des dropdowns
    this.sexe = [];
    this.sexe.push({label: 'Selectionner votre sexe', value:''});
    this.sexe.push({label: 'Femme', value:'F'});
    this.sexe.push({label: 'Homme', value:'H'});

    this.age = [];
    this.age.push({label: 'Selectionner votre tranche d\'âge', value:''});
    this.age.push({label: '0-14 ans', value:'1'});
    this.age.push({label: '15-29 ans', value:'2'});
    this.age.push({label: '30-44 ans', value:'3'});
    this.age.push({label: '45-59 ans', value:'4'});
    this.age.push({label: '60 et plus', value:'5'});

    this.experience = [];
    this.experience.push({label: 'Votre expérience dans la musique', value:''});
    this.experience.push({label: 'Débutant (< 1 ans)', value:'1'});
    this.experience.push({label: 'Confirmé (2-5 ans)', value:'2'});
    this.experience.push({label: 'Expert (> 5 ans', value:'3'});
    
  }

}
